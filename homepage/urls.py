from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.sabeb, name='index'),
    path('portfolio', views.portfolio, name='portfolio'),
    path('create_friend', views.friend_form_view, name='create_friend'),
    path('friends', views.show_friends, name='show_friends'),
    # dilanjutkan ...
]

from django.db import models

# Create your models here.
class Year(models.Model):
    year = models.CharField(max_length=4)
    def __str__(self):
        return self.year

class Friend(models.Model):
    name = models.CharField(max_length=255)
    hobby = models.CharField(max_length=255)
    favFood = models.CharField(max_length=255)
    favDrink = models.CharField(max_length=255)
    year = models.ForeignKey(Year,on_delete=models.CASCADE)
    def __str__(self):
        return self.name

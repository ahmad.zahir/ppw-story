from django import forms

from .models import Friend, Year

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = [
            'name',
            'hobby',
            'favFood',
            'favDrink',
            'year'
        ]

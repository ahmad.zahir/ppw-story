from django.shortcuts import render
from .forms import FriendForm
from .models import Friend, Year
# Create your views here.
def sabeb(request):
    return render(request, 'index.html')

def portfolio(request):
    return render(request, 'portfolio.html')

def friend_form_view(request):
    form = FriendForm(request.POST or None)
    if form.is_valid():
        form.save()
    context = {
        'form': form
    }
    return render(request, "create_friend.html", context)

def show_friends(request):
    friends=Friend.objects.values_list('name','hobby','favFood','favDrink','year')
    context = {
        'friends' : friends
    }
    return render(request, "friends.html", context)

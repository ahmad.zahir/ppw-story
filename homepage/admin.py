from django.contrib import admin

# Register your models here.
from .models import Friend, Year

admin.site.register(Friend)
admin.site.register(Year)
